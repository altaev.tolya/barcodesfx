package site.barsukov.barcodefx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.RequiredArgsConstructor;
import net.rgielen.fxweaver.core.FxmlView;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import site.barsukov.barcodefx.context.DataMatrixContext;
import site.barsukov.barcodefx.model.AboutTextEnum;
import site.barsukov.barcodefx.services.AboutService;
import site.barsukov.barcodefx.services.CatalogService;
import site.barsukov.barcodefx.services.PDFCreateService;
import site.barsukov.barcodefx.services.PDFTemplatesCreateService;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@FxmlView("/fxml/print_packs.fxml")
public class PrintPacksController implements Initializable {
    static final Logger logger = Logger.getLogger(PrintPacksController.class);
    private final AboutService aboutService;
    private final CatalogService catalogService;
    public CheckBox endToEndNumbering;
    public TextField packSize;
    private DataMatrixContext dataMatrixContext;
    private boolean useTemplate;
    private Boolean success;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void printButtonAction(ActionEvent actionEvent) {
        try {
            success = false;
            printPacks();
            success = true;
        } catch (IOException e) {
            logger.error("Error while printing packs", e);
        } finally {
            Stage stage = (Stage) packSize.getScene().getWindow();
            stage.getScene().setUserData(success);
            stage.close();
        }
    }

    private void printPacks() throws IOException {
        File csvFile = new File(dataMatrixContext.getCsvFileName());
        List<String> lines = FileUtils.readLines(csvFile, "UTF-8")
                .stream()
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.toList());
        int packSizeInt = Integer.parseInt(packSize.getText());
        int totalSize = lines.size();
        dataMatrixContext.setMarkNumberStart(1);

        for (int i = 1; i <= totalSize; i += packSizeInt) {
            dataMatrixContext.setRangeEnabled(true);
            dataMatrixContext.setRangeFrom(Integer.toString(i));
            int rangeTo = i + packSizeInt - 1;
            if (rangeTo > totalSize) {
                rangeTo = totalSize;
            }
            dataMatrixContext.setRangeTo(Integer.toString(rangeTo));
            if (endToEndNumbering.isSelected()) {
                dataMatrixContext.setMarkNumberStart(i);
                dataMatrixContext.setTotalPageNumber(totalSize);
            }
            if (useTemplate) {
                PDFTemplatesCreateService pdfTemplatesCreateService = new PDFTemplatesCreateService(dataMatrixContext, catalogService);
                pdfTemplatesCreateService.performAction();
            } else {
                PDFCreateService pdfCreateService = new PDFCreateService(dataMatrixContext);
                pdfCreateService.performAction();
            }
        }
    }

    public void setDataMatrixContext(DataMatrixContext dataMatrixContext) {
        this.dataMatrixContext = dataMatrixContext;
    }

    public void setUseTemplate(boolean useTemplate) {
        this.useTemplate = useTemplate;
    }

    public void openAboutPackPrintAction(MouseEvent mouseEvent) {
        aboutService.openAboutWindow(AboutTextEnum.PACK_PRINTING);
    }
}
