package site.barsukov.barcodefx.model.enums;

import lombok.Getter;
import org.springframework.lang.Nullable;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public enum GoodCategory {
    SHOES("Обувь", 13, 88,"91","92"),
    ATP("АТП", 7, 0,"91",null),
    TIRES("Шины", 13, 44,"91","92"),
    CLOTHES("Одежда", 13, 44,"91","92"),
    PERFUMERY("Парфюм", 13, 44,"91","92"),
    MILK("Молоко", 6, 0,"93",null);

    private static Map<String, GoodCategory> names = Stream.of(GoodCategory.values())
            .collect(Collectors.toMap(GoodCategory::getShortName, s -> s));

    private String shortName;
    private int serialLength;
    private int tailLength;
    private String checkKeyAI;
    private String cryptoAI;


    GoodCategory(String shortName, int serialLength, int tailLength, String checkKeyAI, String cryptoAI) {
        this.shortName = shortName;
        this.serialLength = serialLength;
        this.tailLength = tailLength;
        this.checkKeyAI = checkKeyAI;
        this.cryptoAI = cryptoAI;
    }

    public static GoodCategory getByShortName(String name) {
        return names.get(name);
    }

    @Nullable
    public static GoodCategory getByName(String name) {
        if (Objects.isNull(name)) {
            return null;
        }

        return Arrays.stream(GoodCategory.values())
                .filter(e -> e.name().equals(name))
                .findFirst().orElse(null);
    }


    @Override
    public String toString() {
        return shortName;
    }
}
