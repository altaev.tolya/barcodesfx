package site.barsukov.barcodefx.props;

import site.barsukov.barcodefx.model.enums.SysPropType;
import site.barsukov.barcodefx.services.PickingOrderService;

public class JsonLastStateProps extends JsonBaseProp {
    public enum LastStateProp implements IProp {

        CATEGORY("Последняя выбранная категория", "SHOES"),
        LABEL_HEIGHT("Высота этикетки", "113"),
        LABEL_WIDTH("Ширина этикетки", "164"),
        LABEL_TEXT("Текст на этикетке", ""),
        LABEL_START_NUMBER("Начинать нумерацию с", "1"),
        LABEL_NUMERATE("Нумеровать", "false"),
        LABEL_SIZE_A4("Этикетка А4", "true"),
        LABEL_SIZE_TERMOPRINTER("Этикетка термопринтера", "false"),
        LABEL_HORIZONTAL("Этикетка горизонтальная", "true"),
        LABEL_VERTICAL("Этикетка вертикальная", "false"),

        AGGREGATION_ORG_NAME("Агрегация название организации", ""),
        AGGREGATION_DOC_NUM("Агрегация номер документа", ""),
        AGGREGATION_IS_IP("Агрегация признак ИП ", "false"),
        AGGREGATION_INN("Агрегация ИНН", ""),

        PICKING_ORDER_LAST_COMMENT("Прошлый комментарий", ""),
        PICKING_ORDER_LAST_STATUS("Прошлый статус", ""),
        PICKING_ORDER_LAST_TYPE("Прошлый тип кодов", "KM"),
        PICKING_ORDER_LAST_ENABLE_PRINTING("Печатать", "false"),
        PICKING_ORDER_LAST_PRINT_PROFILE("Профиль печати", ""),

        AGG_MULTI_CREATE_INN("Прошлый ИНН организации", ""),
        AGG_MULTI_CREATE_ORG_NAME("Прошлое название организации", ""),
        AGG_MULTI_CREATE_CHANGE_STATUS("Прошлый переключатель смены статуса", "false"),
        AGG_MULTI_CREATE_NEW_STATUS("Прошлый статус", ""),
        AGG_MULTI_SAVE_DIRECTORY("Прошлая папка для сохранения агрегатов", PickingOrderService.LOCAL_PICKING_ORDERS_PATH);

        private final String descr;
        private final String defaultValue;
        private final SysPropType sysPropType = SysPropType.LAST_STATE;

        LastStateProp(String descr, String defaultValue) {
            this.descr = descr;
            this.defaultValue = defaultValue;
        }

        public String getDescr() {
            return descr;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public SysPropType getSysPropType() {
            return sysPropType;
        }
    }

}
